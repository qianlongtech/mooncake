(function ($) {
    "use strict";

    function Game() {
        this.$loader = $(".loader");
        this.$level = $(".level");
        this.$win = $(".win");
        this.$fail = $(".fail");
        this.$timer = $(".timer");
        this.$counter = $(".counter");
        this.$message = null;
        this.timerID = 0;
        this.count = 0;
        this.lang = {
            loader: [
                "Loading",
                "网速太慢，刷新试试？"
            ]
        };
        this.module = {
            loader: '<div class="cover"></div>' +
                '<div class="pacman">' +
                '<div class="pacman1"></div>' +
                '<div class="pacman2"></div>' +
                '<div class="pacman3"></div>' +
                '<div class="pacman4"></div>' +
                '<div class="pacman5"></div>' +
                '</div>' +
                '<div class="message"></div>'
        };
        this.source = {
            image: [],
            audio: []
        };
        this.init = function () {
            this.$loader.html(this.module.loader);
            this.$message = $(".message");
            this.timer(false);
            this.counter(false);
        };
        this.start = function () {
            var game = this;
            game.init();
            game.loadSource(function (x) {
                if (x) {
                    game.$loader.hide();
                    game.level(0);
                    $(".home").children().each(function (index, element) {
                        $(element).delay(100 * index).fadeIn();
                    });
                }
            });
        };
        this.end = function (result) {
            var r,
                x;
            r = (result === true) ? this.$win : this.$fail;
            x = this.$level.index(r);
            this.level(x);
        };
        this.level = function (index) {
            this.$level.removeClass("level-active");
            this.$level.eq(index).addClass("level-active");
        };
        this.timer = function (on) {
            var game = this,
                during = 60,
                countTimeStop = function () {
                    clearTimeout(game.timerID);
                    game.$timer.html(60);
                },
                countTime = function () {
                    during -= 1;
                    game.$timer.html(during);
                    game.timerID = setTimeout(countTime, 1000);
                    if (during < 0) {
                        countTimeStop();
                    }
                };
            if (on) {
                countTime();
            } else {
                countTimeStop();
            }
        };
        this.counter = function (on) {
            var game = this;
            if (on) {
                game.$counter.html(game.count);
            } else {
                game.count = 0;
                game.counter(true);
            }
        };
        this.setSource = function (source) {
            this.source.image = source.image;
            this.source.audio = source.audio;
        };
        this.loadSource = function (callback) {
            var game = this,
                source = [
                    game.source.image,
                    game.source.audio
                ],
                success = [],
                error = [],
                sourceLength = source[0].length + source[1].length;
            game.$message.html(game.lang.loader[0]);
            $.each(source, function (index, element) {
                $.each(source[index], function (i, e) {
                    game.loadSourceDetail(index, e, function (r, s) {
                        if (r) {
                            error.push(s);
                        } else {
                            success.push(s);
                        }
                        game.$message.html(Math.round((success.length / sourceLength) * 100) + "%");
                        if (success.length === sourceLength) {
                            callback(true);
                        }
                        if (error.length > 0) {
                            game.$message.html(game.lang.loader[1]);
                        }
                        if ((success.length + error.length) === sourceLength) {
                            window.console.log("success: " + success);
                            window.console.log("error: " + error);
                        }
                    });
                });
            });
        };
        this.loadSourceDetail = function (type, src, callback) {
            var path,
                xhr;
            path = type ? "audio/" : "image/";
            if (window.XMLHttpRequest) {
                xhr = new window.XMLHttpRequest();
            } else if (window.ActiveXObject) {
                xhr = new window.ActiveXObject("Microsoft.XMLHTTP");
            }
            xhr.open("GET", path + src, true);
            xhr.send();
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        callback(0, src);
                    } else {
                        callback(1, src);
                    }
                }
            };
        };
        this.ajaxSetup = function () {
            $.ajaxSetup({
                beforeSend: function () {
                    this.$loader.show();
                },
                complete: function () {
                    this.$loader.hide();
                }
            });
        };
        this.shareWeixin = function (result, arg) {
            var desc = [
                "月饼太好吃了，败给了这几个馋鬼。各位老板请我吃月饼吧！",
                "你一共赶走小馋鬼" + arg + "次，任务达成！赶快分享吧！"
            ],
                obj = {
                    title: "守护月饼大战",
                    desc: "老板发的月饼，想偷吃？没那么容易！快来参加守护月饼大战！",
                    link: window.location.href.split("#")[0],
                    imgUrl: "http://comic.qianlong.com/m/game925/image/wx.jpg",
                    type: "link",
                    dataUrl: "",
                    success: function () {
                        _hmt.push(["_trackEvent", "game925", "share", "weixin"]);
                    }
                };
            if (result) {
                obj.desc = desc[1];
            } else {
                obj.desc = desc[0];
            }
            wx.onMenuShareAppMessage(obj);
            wx.onMenuShareQQ(obj);
            wx.onMenuShareWeibo(obj);
            wx.onMenuShareTimeline(obj);
        };
    }

    var game,
        lives,
        timeout,
        interval,
        hand = [
            {
                start: {
                    top: "19%",
                    left: "-30%"
                },
                end: {
                    top: "0",
                    left: "-65%"
                }
            },
            {
                start: {
                    top: "19%",
                    right: "-30%"
                },
                end: {
                    top: "0",
                    right: "-65%"
                }
            },
            {
                start: {
                    top: "70%",
                    left: "-30%"
                },
                end: {
                    top: "88%",
                    left: "-65%"
                }
            },
            {
                start: {
                    top: "70%",
                    right: "-30%"
                },
                end: {
                    top: "88%",
                    right: "-65%"
                }
            }
        ],
        cake = [
            {
                start: {
                    top: "23%",
                    left: "2%"
                },
                end: {
                    top: "-3%",
                    left: "-50%"
                }
            },
            {
                start: {
                    top: "23%",
                    right: "2%"
                },
                end: {
                    top: "-3%",
                    right: "-50%"
                }
            },
            {
                start: {
                    top: "50%",
                    left: "2%"
                },
                end: {
                    top: "80%",
                    left: "-50%"
                }
            },
            {
                start: {
                    top: "50%",
                    right: "2%"
                },
                end: {
                    top: "80%",
                    right: "-50%"
                }
            }
        ];

    function init() {
        lives = 4;
        $(".ready1").show();
        $(".ready2").attr("src", "image/text5.gif").show();
        $(".ready3").hide();
        $(".hand").each(function () {
            $(this).removeClass("moveback");
            $(this).css(hand[$(".hand").index($(this))].end);
        });
        $(".cake").each(function () {
            $(this).removeClass("stolen");
            $(this).css(cake[$(".cake").index($(this))].start);
        });
    }

    function moveforward() {
        var $hand, $cake, $h, $c, index, $hands, $cakes, i, v;
        $hands = $(".hand");
        $cakes = $(".cake");
        $hand = $(".hand:not(.moveback)");
        $cake = $(".cake:not(.stolen)");
        index = Math.floor(Math.random() * $hand.length);
        i = Math.floor(Math.random() * 5) + 1;
        $h = $hand.eq(index);
        $c = $cake.eq(index);
        $h.attr("src", "image/hand" + i + ".png");
        if (lives > 1) {
            v = 700;
            $h.animate(hand[$hands.index($h)].start, 400);
        } else {
            v = 200;
            $h.animate(hand[$hands.index($h)].start, 100);
        }
        $c.addClass("aimed");
        setTimeout(function () {
            if ($c.hasClass("aimed")) {
                $h.addClass("moveback");
                $c.addClass("stolen").removeClass("aimed");
                $h.animate(hand[$hands.index($h)].end, 200);
                $c.animate(hand[$cakes.index($c)].end, 200);
                lives -= 1;
            }
            if (lives <= 0) {
                setTimeout(function () {
                    game.end(false);
                    game.shareWeixin(false, game.count);
                    $(".bgm").attr({
                        "src": "audio/fail.mp3",
                        "loop": false
                    });
                    clearInterval(interval);
                    clearTimeout(timeout);
                }, 200);
            }
        }, v);
    }

    function tabback(event) {
        var $h, $hand, $c, $cake, $core, index, posX, posY, paID, $player;
        $h = $(event.target);
        $hand = $(".hand");
        $cake = $(".cake");
        $core = $(".core");
        $player = $(".effect");
        index = $hand.index($h);
        $c = $cake.eq(index);
        if (!$h.hasClass("moveback")) {
            game.count += 1;
            game.counter(true);

            posX = event.targetTouches[0].clientX - 50;
            posY = event.targetTouches[0].clientY - 40;
            paID = setTimeout(function () {
                $(".pa.pa" + paID).remove();
            }, 500);
            $core.append('<img class="pa pa' + paID + '" src="image/pa.png">');
            $core.find(".pa" + paID).css({
                top: posY,
                left: posX
            });
            $player.attr({
                "src": "audio/pa.mp3",
                "loop": false
            });

            $h.addClass("moveback");
            $h.animate(hand[index].end, 200);
            $c.removeClass("aimed");
            setTimeout(function () {
                $h.removeClass("moveback");
            }, 200);
        }
    }

    function core() {
        game.level(1);
        setTimeout(function () {
            $(".ready1").hide();
            $(".ready2").attr("src", "").hide();
            $(".ready3").show();
        }, 3000);
        setTimeout(function () {
            game.level(2);
            game.timer(true);
            game.counter(true);
            $(".bgm").attr({
                "src": "audio/bgm.mp3",
                "loop": true
            });
        }, 4000);
        setTimeout(function () {
            interval = setInterval(function () {
                moveforward();
            }, 1000);
        }, 3000);
        timeout = setTimeout(function () {
            game.end(true);
            game.shareWeixin(true, game.count);
            $(".bgm").attr({
                "src": "audio/win.mp3",
                "loop": false
            });
            clearInterval(interval);
        }, 64000);
    }

    function replay() {
        game.init();
        init();
        core();
    }

    function bind() {
        $(".b1").on("click", core);
        $(".b2").on("click", replay);
        $(".hand").each(function () {
            this.addEventListener("touchstart", tabback);
        });
        $("body").on("touchmove", function (event) {
            event.preventDefault();
        });
    }

    game = new Game();
    game.setSource({
        image: [
            "bg1.jpg",
            "bg2.jpg",
            "bg3.jpg",
            "bg4.jpg",
            "cover1.png",
            "button1.png",
            "button2.png",
            "text1.png",
            "text2.png",
            "text3.png",
            "text4.png",
            "text5.gif",
            "text6.png",
            "text7.png",
            "text8.png",
            "text9.png",
            "cake1.png",
            "cake2.png",
            "cake3.png",
            "cake4.png",
            "hand1.png",
            "hand2.png",
            "hand3.png",
            "hand4.png"
        ],
        audio: [
            "bgm.mp3",
            "pa.mp3",
            "win.mp3",
            "fail.mp3"
        ]
    });
    game.start();
    init();
    bind();

}(window.jQuery));